package conversionAndStringFunc;

public class NumberConversionFunctions {

    /*2. Написать и протестировать функции преобразования:
- целого  числа в строку
- вещественного числа в строку
- строки в целое число
- строки в вещественное число*/


    public static String integerToString ( int number){
            return Integer.toString(number);
        }

    public static String doubleToString(double number) {
        return Double.toString(number);
    }

    public static int stringsToInteger(String number) {
        return Integer.parseInt(number);
    }

    public static double stringsToDouble(String number) {
        return Double.parseDouble(number);
    }


}
