package conversionAndStringFunc;

public class InOneStringChars {
    /*1. Вывести в одну строку символы:
- английского алфавита от ‘A’ до ‘Z’
- английского алфавита от ‘z’ до ‘a’
- русского алфавита от ‘а’ до ‘я’
- цифры от ‘0’ до ‘9’
- печатного диапазона таблицы ASCII*/

    public static String returnDictionaryUpperCase() {
        String strDictionaryUpperCase = "";
        for (char i = 65; i <= 90; i++) {
            strDictionaryUpperCase += i;
        }
        return strDictionaryUpperCase;
    }

    public static String returnDictionaryLowerCaseRevers() {
        String strDictionaryLowerCaseRevers = "";
        for (char i = 122; i >= 97; i--) {
            strDictionaryLowerCaseRevers += i;
        }
        return strDictionaryLowerCaseRevers;
    }

    public static String returnNumberASCIIFromZeroToNine() {
        String strNumberASCIIFromZeroToNine = "";
        for (char i = 48; i <= 57; i++) {
            strNumberASCIIFromZeroToNine += i;
        }
        return strNumberASCIIFromZeroToNine;
    }

    public static String returnAllPrintedASCIIchars() {
        String strAllPrintedASCIIchars = "";
        for (char i = 0; i < 255; i++) {
            strAllPrintedASCIIchars += i;
        }
        return strAllPrintedASCIIchars;
    }


}

