package stringFunctions;
/*Оставить в строке только один экземпляр каждого встречающегося символа.*/
public class OneInstanceOfEachCharacterEncountered {

    public static String returnFormattedString(String str) {
        char[] charsArray = str.toCharArray();
        StringBuilder sb = new StringBuilder();

        boolean repeatedChar;
        for (int i = 0; i < charsArray.length; i++) {
            repeatedChar = false;
            for (int j = i + 1; j < charsArray.length; j++) {
                if (charsArray[i] == charsArray[j]) {
                    repeatedChar = true;
                    break;
                }
            }

            if (!repeatedChar) {
                sb.append(charsArray[i]);
            }
        }
        return String.valueOf(sb);
    }

}
