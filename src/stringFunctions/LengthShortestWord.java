package stringFunctions;
/*- Дана строка, состоящая из слов, разделенных пробелами и знаками препинания.
 Определить длину самого короткого слова.*/

public class LengthShortestWord {
    public static int returnLengthShortestWord(String inputString) {
        String[] array = inputString.split(" |,|:|;|\\?|!|\\.|-");

        int lengthShortestWord = array[0].length();
        for (String word : array) {
            if (word.length() != 0) {
                if (word.length() < lengthShortestWord) {
                    lengthShortestWord = word.length();
                }
            }
        }
        return lengthShortestWord;
    }
}
