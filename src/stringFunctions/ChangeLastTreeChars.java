package stringFunctions;
/*
 * - Дан массив слов. Заменить последние три символа слов, имеющих заданную длину на символ "$"*/

import java.util.Arrays;

public class ChangeLastTreeChars {

    public static String[] returnArrayWithChangedWord(String[] str, int LengthWords) {
        for (int i = 0; i < str.length; i++) {

            if (str[i].length() == LengthWords) {
                char[] tempArr = str[i].toCharArray();
                tempArr[tempArr.length - 1] = '$';
                tempArr[tempArr.length - 2] = '$';
                tempArr[tempArr.length - 3] = '$';
                str[i] = String.valueOf(tempArr);
            }
        }
        return str;
    }
}
