package stringFunctions;
/*Подсчитать количество слов во введенной пользователем строке*/

public class NumberOfWordsInUserEnteredStr {

    public static int returnNumberInputWords(String inputString) {
        if (inputString == null || inputString.isEmpty()) {
            return 0;
        }
        String[] words = inputString.split("\\s+");
        return words.length;
        //
    }
}
