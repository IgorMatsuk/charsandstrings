package stringFunctions;

/*- Добавить в строку пробелы после знаков препинания, если они там отсутствуют.*/
public class AddSpacesToTheString {


    public static String returnFormattedString(String str) {
        str = str
                //.replaceAll("(?<=\\S)\\p{Punct}", "$0")
                .replaceAll("\\p{Punct}(?=\\S)", "$0 ");
        return str;
    }
}
