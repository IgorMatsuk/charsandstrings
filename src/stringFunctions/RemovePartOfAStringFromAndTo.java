package stringFunctions;
/*Удалить из строки ее часть с заданной позиции и заданной длины.*/

public class RemovePartOfAStringFromAndTo {

    public static String returnRemovedStringFromTo(String inputString, int from, int length) {
        if (inputString == null || inputString.isEmpty()) {
            return "String is empty";
        }
        length += from;
        return new StringBuilder(inputString).delete(from, length).toString();
    }
}
