package stringFunctions;
//+ Перевернуть строку, т.е. последние символы должны стать первыми, а первые последними.
public class ReversString {
    public static String returnReversedString(String str){
        return new StringBuilder(str).reverse().toString();
    }
}
