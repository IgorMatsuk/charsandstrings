package stringFunctions;
/*В заданной строке удалить последнее слово
* --нужно переделать через регулярные выражения, с учетом всех знаков табуляции*/

public class RemoveLastWordInLine {

    public static String returnStringWithoutLastWord(String inputString){
        if (inputString == null || inputString.isEmpty()) {
            return "String is empty";
        }
        int index=inputString.lastIndexOf(" ");
        return inputString.substring(0,index);

    }

}
