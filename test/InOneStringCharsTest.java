import conversionAndStringFunc.InOneStringChars;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InOneStringCharsTest{


    @Test
    public void Dictionary_Length() {
        int lengthDictionary = InOneStringChars.returnDictionaryUpperCase().length();
        int latinAlphabetConsists = 26;
        assertEquals(lengthDictionary,latinAlphabetConsists);
    }
    @Test
    public void Dictionary_Upper_Case(){
        String string = InOneStringChars.returnDictionaryUpperCase();
        String latinDictionary = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        assertEquals(string,latinDictionary);


    }

    @Test
    public void return_Dictionary_Lower_Case_Revers() {
        String string = InOneStringChars.returnDictionaryLowerCaseRevers();
        String dictionaryLowCaseRevers = "zyxwvutsrqponmlkjihgfedcba";
        assertEquals(string,dictionaryLowCaseRevers);
    }

    @Test
    public void return_Number_ASCII_From_Zero_To_Nine() {
        String string = InOneStringChars.returnNumberASCIIFromZeroToNine();
        String result= "0123456789";
        assertEquals(string,result);

    }

    @Test
    public void check_The_Number_Of_Items_ASCII() {
        int value = InOneStringChars.returnAllPrintedASCIIchars().length();
        int result = 255;
        assertEquals(value,result);
    }
}