package t_stringFunctions;

import org.junit.jupiter.api.Test;
import stringFunctions.LengthShortestWord;

import static org.junit.jupiter.api.Assertions.*;

class LengthShortestWordTest {

    @Test
    void returnLengthShortestWord() {
    String str = "There are five of us - my parents, my elder brother,".concat(
            " my baby sister and me. First, meet my mum and dad, Jane and Michael.");
    int result = 2;
    assertEquals(LengthShortestWord.returnLengthShortestWord(str),result);
    }
}