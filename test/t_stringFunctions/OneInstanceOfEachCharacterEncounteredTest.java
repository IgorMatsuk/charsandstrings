package t_stringFunctions;

import org.junit.jupiter.api.Test;
import stringFunctions.OneInstanceOfEachCharacterEncountered;

import static org.junit.jupiter.api.Assertions.*;

class OneInstanceOfEachCharacterEncounteredTest {

    @Test
    void returnFormattedString() {
        String str = "Hello people";
        String result = "H ople";
        assertEquals(OneInstanceOfEachCharacterEncountered.returnFormattedString(str), result);
    }
}