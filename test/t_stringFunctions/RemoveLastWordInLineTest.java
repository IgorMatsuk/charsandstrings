package t_stringFunctions;

import org.junit.jupiter.api.Test;
import stringFunctions.RemoveLastWordInLine;

import static org.junit.jupiter.api.Assertions.*;

class RemoveLastWordInLineTest {

    @Test
    void returnStringWithoutLastWord() {
        String inputString = "Hello people";
        String result = "Hello";
        assertEquals(RemoveLastWordInLine.returnStringWithoutLastWord(inputString),result);
    }
}