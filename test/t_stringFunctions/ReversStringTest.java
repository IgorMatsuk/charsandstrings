package t_stringFunctions;

import org.junit.jupiter.api.Test;
import stringFunctions.ReversString;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReversStringTest {

    @Test
    public void returnReversedString(){
        String str = "Hello world!";
        String result = "!dlrow olleH";
        assertEquals(ReversString.returnReversedString(str),result);
    }
}
