package t_stringFunctions;

import org.junit.jupiter.api.Test;
import stringFunctions.ChangeLastTreeChars;

import static org.junit.jupiter.api.Assertions.*;

class ChangeLastTreeCharsTest {

    @Test
    void returnArrayWithChangedWord() {
        String[] wordsArray = {"Odessa", "Lviv", "Kiev", "Kharkov"};
        String [] resultArray = {"Odessa", "Lviv", "Kiev", "Khar$$$"};
        assertArrayEquals(ChangeLastTreeChars.returnArrayWithChangedWord(wordsArray,7),resultArray);
    }
}