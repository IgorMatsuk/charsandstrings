package t_stringFunctions;

import org.junit.jupiter.api.Test;
import stringFunctions.AddSpacesToTheString;

import static org.junit.jupiter.api.Assertions.*;

class AddSpacesToTheStringTest {

    @Test
    void returnFormattedString() {
        String str = "Today,a good, day:Yeah!!!";
        String result = "Today, a good, day: Yeah! ! !";
        assertEquals(AddSpacesToTheString.returnFormattedString(str),result);
    }
}