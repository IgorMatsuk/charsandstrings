package t_stringFunctions;

import org.junit.jupiter.api.Test;
import stringFunctions.RemovePartOfAStringFromAndTo;

import static org.junit.jupiter.api.Assertions.*;

class RemovePartOfAStringFromAndToTest {

    @Test
    void returnRemovedStringFromTo() {
        String inputString = "Hello world!";
        String result = "world!";
        assertEquals(RemovePartOfAStringFromAndTo.returnRemovedStringFromTo(inputString,0,6),result);

    }
}