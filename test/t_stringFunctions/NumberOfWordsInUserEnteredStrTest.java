package t_stringFunctions;

import org.junit.jupiter.api.Test;
import stringFunctions.NumberOfWordsInUserEnteredStr;

import static org.junit.jupiter.api.Assertions.*;

class NumberOfWordsInUserEnteredStrTest {

    @Test
    void returnNumberInputWords() {
        String str = "If you fall asleep now, you will dream. If you study now, you will live your dream.";
        int result = 17;
        assertEquals(NumberOfWordsInUserEnteredStr.returnNumberInputWords(str),result);

    }
}