import conversionAndStringFunc.NumberConversionFunctions;
import org.junit.jupiter.api.Test;

class NumberConversionFunctionsTest{

    @Test
    public void integer_To_String_True() {
        String i = NumberConversionFunctions.integerToString(10);
        String result = "10";
        assertEquals(i, result);
    }

    private void assertEquals(String i, String result) {
    }

    @Test
    public void double_To_String_True() {
        String i = NumberConversionFunctions.doubleToString(10.45);
        String result = "10.45";
        assertEquals(i, result);

    }

    @Test
    public void strings_To_Integer() {
        int i = NumberConversionFunctions.stringsToInteger("122");
        int result = 122;
        //assertEquals(i, result);
    }

    @Test
    public void strings_To_Double() {
        double i = NumberConversionFunctions.stringsToDouble("122.56");
        double result = 122.56;
       // assertEquals(i, result);
    }


}