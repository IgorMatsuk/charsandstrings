package param_tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static conversionAndStringFunc.NumberConversionFunctions.doubleToString;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class DoubleToStringParamTest {
    private double valueA;
    private String expected;

    public DoubleToStringParamTest(String expected, Double valueA) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"10.0", 10.0},
                {"12.0", 12.0}
        });
    }

    @Test
    public void doubleToStringTest() {
        assertEquals(expected, doubleToString(valueA));
    }
}
