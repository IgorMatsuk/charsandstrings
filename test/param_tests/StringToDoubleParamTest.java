package param_tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static conversionAndStringFunc.NumberConversionFunctions.stringsToDouble;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class StringToDoubleParamTest {
    private String valueA;
    private double expected;

    public StringToDoubleParamTest(double expected, String valueA) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {11.0, "11.0"},
                {12.0, "12.0"}
        });
    }

    @Test
    public void stringToDoubleTest() {
        assertEquals(expected, stringsToDouble(valueA));
    }
}
