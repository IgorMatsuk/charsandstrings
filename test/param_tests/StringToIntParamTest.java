package param_tests;

import conversionAndStringFunc.NumberConversionFunctions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class StringToIntParamTest {
    private String valueA;
    private int expected;

    public StringToIntParamTest(int expected, String valueA) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {10, "10"},
                {12, "12"}
        });
    }

    @Test
    public void stringToIntTest() {
        assertEquals(expected, NumberConversionFunctions.stringsToInteger(valueA));

    }
}