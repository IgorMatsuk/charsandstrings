package param_tests;

import conversionAndStringFunc.NumberConversionFunctions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class IntToStringParamTest {
    private int valueA;
    private String expected;

    public IntToStringParamTest(String expected, Integer valueA) {
        this.valueA = valueA;
        this.expected = expected;
    }


    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"10", 10},
                {"12", 12}

        });
    }

    @Test
    public void intToStringTest() {
        assertEquals(expected, NumberConversionFunctions.integerToString(valueA));

    }

}
